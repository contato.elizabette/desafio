# Desafio técnico
## Desafio técnico divertido Bete VS Labs 💙.
### Para executar o projeto seguem abaixo os comandos:

1. `Para produzir a imagem docker, executar:` *make build-image*
2. `Rodar o comando make:` *make run-local*

### Testes unitários

1. `Para rodar os testes unitários:` *make test*

### Demais comandos:

1. `Inicializar container:` *make start-container*
2. `Para finalizar o container`: *make stop-container*

### Acesso:

A aplicação pode ser acessada em http://127.0.0.1

Documentação disponível em http://127.0.0.1/docs
