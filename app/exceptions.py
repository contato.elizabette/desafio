class FriendNotFound(Exception):
    def __init__(self, name):
        self.message = f'{name} not found'
        self.status_code = 404


class LevelNotSupported(Exception):
    def __init__(self, level):
        self.message = f'{level}, cannot be evaluated'
        self.status_code = 404


class ExistingRegistration(Exception):
    def __init__(self, name):
        self.message = f'Existing registration for name {name}'
        self.status_code = 409
