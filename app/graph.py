from typing import Dict, List, Tuple
from collections import deque
from app.exceptions import (
    FriendNotFound,
    LevelNotSupported,
    ExistingRegistration
)

initial_friends: Dict[str, List[str]] = {
    'Ana': ['Maria', 'Vinicius', 'Joao', 'Carlos'],
    'Maria': ['Ana', 'Vinicius'],
    'Vinicius': ['Maria', 'Ana'],
    'Luiza': ['Joao'],
    'Joao': ['Ana', 'Luiza'],
    'Carlos': ['Ana']
}


def create_edges(data: Dict):
    """ Creating the edges of initial_friends """

    edges: List[Tuple[str, str]] = []
    for key in data.keys():
        for vertex in data[key]:
            edges.append((key, vertex))
    return edges


class Graph:
    """Generate a graph from a network of friends."""

    def __init__(self, edges, is_directed=True):
        self.adjacent = {}
        self.edges = edges
        self.is_directed = is_directed
        self.add_edges(edges)

    def add_bow(self, key, value):
        """ Adds the bow to the edges """

        if key not in list(self.adjacent.keys()):
            self.adjacent[key] = [value]
        else:
            if value not in self.adjacent[key]:
                self.adjacent[key].append(value)
        if not self.is_directed:
            if key not in list(self.adjacent.keys()):
                self.adjacent[value] = [key]
            else:
                if key not in self.adjacent[value]:
                    self.adjacent[value].append(key)

    def add_edges(self, edges):
        """ Add edges """
        for origin, destiny in edges:
            self.add_bow(origin, destiny)

    def get_vertex(self):
        """ Return the nodes(vertex) """
        return list(self.adjacent.keys())


def neighbors_search(graph, start):

    """ Search friends of friends """

    dict_aux = graph.adjacent
    vertex = graph.get_vertex()
    aux = [False] * len(vertex)
    visited = dict(zip(vertex, aux))
    queue = deque()
    queue += [start]
    visited[start] = True
    while queue:
        valor = queue.popleft()
        for i in dict_aux[valor]:
            if not visited.get(i, []):
                queue += [i]
                visited[i] = True
        return visited


def search_level(graph, start, level):
    """ Search for close friends or friends of a friend """

    neighbors = neighbors_search(graph, start)
    level1 = []
    level2 = []
    for key, value in neighbors.items():
        if value and key != start:
            level1.append(key)
        if not value:
            level2.append(key)
    if level == 1:
        return level1
    if level == 2:
        return level2
    else:
        raise LevelNotSupported(level)


def add_person(name, friends):
    """ Add new people to the friends network """

    friend_values = initial_friends.get(name, [])
    if name in initial_friends.keys():
        raise ExistingRegistration(name)
    for friend in friends:
        if friend in initial_friends.keys():
            friend_values.append(friend)
        else:
            raise FriendNotFound(friend)
    initial_friends.update({name: friends})
