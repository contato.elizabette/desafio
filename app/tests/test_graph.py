from app.graph import (
    create_edges,
    Graph,
    search_level,
    add_person,
    initial_friends
)


def test_create_edges():
    dic = {'Maria': ['Ana', 'Vinicius']}

    assert create_edges(dic) == [('Maria', 'Ana'), ('Maria', 'Vinicius')]


def test_adjacent():
    graph = Graph(edges=[('Maria', 'Ana'), ('Maria', 'Vinicius')])

    assert graph.adjacent == {'Maria': ['Ana', 'Vinicius']}


def test_get_vertex():
    graph = Graph(edges=[('Maria', 'Ana'), ('Maria', 'Vinicius')])

    assert graph.get_vertex() == ['Maria']


def test_search_level():
    graph = Graph(edges=[('Luiza', 'Joao')])

    assert search_level(graph=graph, start='Luiza', level=1) == ['Joao']


def test_add_person():
    add_person(name='Bete', friends=['Ana'])
    assert initial_friends['Bete'] == ['Ana']
