from fastapi.testclient import TestClient

from app.main import app


client = TestClient(app)


def test_read_friends():
    response = client.get('/v1/friends/')
    assert response.status_code == 200
    assert isinstance(response.json(), list)


def test_read_neighbors():
    response = client.get('/v1/friends/Luiza/neighbors/?level=1')
    assert response.status_code == 200
    assert response.json() == ['Joao']


def test_not_found_friend():
    response = client.get('/v1/friends/Joaozinho/neighbors/?level=1')
    assert response.status_code == 404
    assert response.json() == {"detail": "Friend not found"}


def test_level_not_supported():
    response = client.get('/v1/friends/Ana/neighbors/?level=0')
    assert response.status_code == 404
    assert response.json() == {"detail": "Level not supported"}


def test_existing_registration():
    response = client.post(
        '/v1/friends/',
        json={'name': 'Ana', 'friends': ['Ana']}
    )
    assert response.status_code == 409
    assert response.json() == {'detail': 'Existing registration for name Ana'}
