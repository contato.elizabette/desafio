from typing import List

from fastapi import FastAPI, HTTPException

from pydantic import BaseModel
from app.exceptions import (
    FriendNotFound,
    LevelNotSupported,
    ExistingRegistration
)

from app.graph import (
    Graph,
    add_person,
    create_edges,
    search_level,
    initial_friends,
    neighbors_search
)

description = """
Desafio técnico divertido. Bete VS Labs 🚀

## Friend's Graph

You can **get friends**.<br />
You can **get friend's neighbors**.<br />
You can **add friend**.
"""

app = FastAPI(
    title='Desafio técnico',
    description=description,
    version='0.0.1',
    contact={
        'name': 'Maria Elizabete',
        'email': 'contato.elizabette@gmail.com',
    },
    license_info={
        "name": "Apache 2.0",
        "url": "https://www.apache.org/licenses/LICENSE-2.0.html",
    },
)


@app.get('/v1/friends/')
async def get_friends():
    """ Route that return all friends """
    edges = create_edges(initial_friends)
    graph = Graph(edges=edges)
    nodes = graph.get_vertex()
    return nodes


@app.get('/v1/friends/{name}/neighbors/')
async def get_neighbors(name: str, level: int):
    """ Route that return neighbors by friends """
    if name.title() not in initial_friends:
        raise HTTPException(status_code=404, detail='Friend not found')
    try:
        edges = create_edges(initial_friends)
        graph = Graph(edges=edges)
        neighbors = search_level(graph=graph, start=name.title(), level=level)
        return neighbors
    except LevelNotSupported:
        raise HTTPException(status_code=404, detail='Level not supported')


class Friendship(BaseModel):
    name: str
    friends: List[str]


@app.post('/v1/friends/')
async def add_friends(friendship: Friendship):
    """ Route to add new friends """
    try:
        add_person(name=friendship.name.title(), friends=friendship.friends)
    except (FriendNotFound, ExistingRegistration) as e:
        raise HTTPException(status_code=e.status_code, detail=e.message)
    edges = create_edges(initial_friends)
    graph = Graph(edges=edges)
    neighbors = neighbors_search(graph=graph, start=friendship.name.title())
    return neighbors
