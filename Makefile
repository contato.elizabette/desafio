test:
	docker exec mycontainer pytest

build-image:
	docker build -t myimage .
	@echo 'image built...'

run-local:
	docker run -d --name mycontainer -p 80:80 myimage
	@echo 'running app...'

start-container:
	docker start mycontainer
	@echo 'system up!'

stop-container:
	docker stop mycontainer
	@echo 'system down!'
